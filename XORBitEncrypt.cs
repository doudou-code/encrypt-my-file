﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace doudou
{
    class XORBitEncrypt : IByteEncrypt
    {
        private byte _slat;
        public XORBitEncrypt(byte xor = 0xDD)
        {
            this._slat = xor;
        }
        public byte[] Decrypt(byte[] buff)
        {
            return this.Encrypt(buff);
        }
        public byte[] Encrypt(byte[] buff)
        {
            var _buff = new byte[buff.Length];
            buff.CopyTo(_buff, 0);

            for (var idx = 0; idx < _buff.Length; idx++)
                _buff[idx] ^= this._slat;

            return _buff;
        }
    }
}
