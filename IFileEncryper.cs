﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace doudou
{
    public interface IByteEncrypt
    {
        byte[] Encrypt(byte[] buff);
        byte[] Decrypt(byte[] buff);
    }
    public interface IFileEncrypt
    {
        void Encrypt(string srcFilepath);

        void Decrypt(string srcFilepath);
    }

    public class DefaultEncryptStream : IFileEncrypt
    {
        private class MetaData
        {
            public MetaData(long pos, long offset, string name,bool isEncrypt)
            {
                this.Pos = pos;
                this.Offset = offset;
                this.FileName = name;
                this.IsEncrypt = isEncrypt;
            }
            
            public static MetaData FromStream(Stream fs)
            {
                var pos = fs.Seek(-10, SeekOrigin.End);
                var buff = new byte[10];
                fs.Read(buff, 0, buff.Length);

                var fileName = Encoding.UTF8.GetString(buff, 0, 6);
                if (!fileName.Equals(MetaData.Magic)) return default(MetaData);

                var metaOffset = BitConverter.ToInt32(buff, 6);

                var metaStart = fs.Length - (10 + metaOffset);

                var buffdata = new byte[metaOffset];
                fs.Seek(metaStart, SeekOrigin.Begin);
                fs.Read(buffdata, 0, buffdata.Length);


                var data = Encoding.UTF8.GetString(buffdata).Split(",");
                var Pos = Convert.ToInt64(data[0]);
                var offset = Convert.ToInt64(data[1]);
                var FileName = data[2];

                return new MetaData(Pos, offset, FileName,true);
            }
            public static string Magic => "DOUDOU";
            public bool IsEncrypt { get; set; }
            public long Pos { get; set; }
            public long Offset { get; set; }
            public string FileName { get; set; }

            public override string ToString()
            {
                return $"{Pos},{Offset},{FileName}";
            }
        }

        private IByteEncrypt _byteEncrypt;
        public DefaultEncryptStream() {
            _byteEncrypt = new XORBitEncrypt(0xDD);
        }

        private byte[] addWeaterMask(FileStream file)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                Bitmap bmp = new Bitmap(800, 800);
                Graphics g = Graphics.FromImage(bmp);
                g.FillRectangle(Brushes.Red, new Rectangle(0, 0, 800, 800));
                g.DrawString($"\n你的文件已被抖抖加密！\nName:{file.Name}\nSize: {file.Length}", 
                    new Font("黑体", 25), 
                    Brushes.Black, 
                    new PointF(20, 100));
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                return ms.ToArray();
            }
        }

        public void Encrypt(string filepath)
        {
            byte[] data, buff, metaBuff;
            using (FileStream fs = new FileStream(filepath, FileMode.Open))
            {
                var fsbuff = new byte[fs.Length];
                fs.Read(fsbuff, 0, fsbuff.Length);
                data = _byteEncrypt.Encrypt(fsbuff);

                buff = this.addWeaterMask(fs);
            }

            using(FileStream stream = new FileStream(Path.ChangeExtension(filepath, ".png"), FileMode.Create))
            {
                stream.Write(buff, 0, buff.Length);
                string metadata = new MetaData(buff.Length, data.Length, filepath, false).ToString();
                stream.Write(data, 0, data.Length);

                metaBuff = Encoding.UTF8.GetBytes(metadata);

                stream.Write(metaBuff, 0, metaBuff.Length);
                stream.Write(Encoding.UTF8.GetBytes(MetaData.Magic));
                stream.Write(BitConverter.GetBytes(metaBuff.Length));
            }

        }

        public void Decrypt(string filepath)
        {
            using (FileStream fs = new FileStream(filepath, FileMode.Open))
            {
                var meta = MetaData.FromStream(fs);
                if (!meta.IsEncrypt)
                    return;

                fs.Seek(meta.Pos, SeekOrigin.Begin);
                var buff = new byte[meta.Offset];
                fs.Read(buff, 0, buff.Length);
                buff = _byteEncrypt.Decrypt(buff);

                using (var stream = new FileStream(meta.FileName, FileMode.Create))
                {
                    stream.Write(buff, 0, buff.Length);
                }
            }
        }
    }
   
}
